<?php

namespace ASW\Utility;

use JetBrains\PhpStorm\ArrayShape;
use Stringable;

/**
 * 操作结果通用类
 * @template T
 */
class ActionResult implements Stringable
{

    /**-----------------------------**/
    /**  Static Region              **/
    /**
     * @var bool 处理结果是否成功
     */
    public bool $success;
    /**
     * @var string 处理结果的文本信息, 通常用来传递错误文本
     */
    public string $message;
    /**
     * @var string 处理结果的内部文本信息, 通常用来传递不交付给用户的内部信息, 例如用于记录详细日志
     */
    public string $innerMessage = '';
    /**
     * @var T 处理结果数据
     */
    public mixed $data;

    /**-----------------------------**/
    /**  Instance Region            **/
    /**-----------------------------**/
    /**
     * @var int 状态码, 默认为0, 一般用负数表达错误码, 正整数表达状态码
     */
    public int $code = 0;

    /**
     * @param bool $success
     * @param string $message
     * @param T|null $data
     * @param int $code
     */
    public function __construct(bool $success, string $message = '', mixed $data = null, int $code = 0)
    {
        $this->success = $success;
        $this->message = $message;
        $this->data    = $data;
        $this->code    = $code;
    }

    /**-----------------------------**/

    public static function fromJson(string $json): ?static
    {
        if (null === $arr = json_decode($json, true)) return null;
        return self::fromArray($arr);
    }

    public static function fromArray(array $arr): static
    {
        $success = boolval($arr['success'] ?? false);
        $message = strval($arr['message'] ?? '');
        $data    = $arr['data'] ?? null;
        $code    = intval($arr['code']) ?? 0;
        return new static($success, $message, $data, $code);
    }

    /**
     * 从 ExecuteResult 转换
     * @param ExecuteResult $result
     * @return $this
     */
    public function fromExecuteResult(ExecuteResult $result): static
    {
        return new static($result->result, $result->info, $result->data, $result->result ? 0 : -1);
    }

    /**
     * 成功
     * @param mixed|null $data
     * @param string $message
     * @param int $code
     * @return static
     */
    public static function success(mixed $data = null, string $message = 'success', int $code = 0): static
    {
        return new static(true, $message, $data, $code);
    }

    /**
     * 失败
     * @param string $message
     * @param mixed|null $data
     * @param int $code
     * @return static
     */
    public static function fail(string $message = 'fail', mixed $data = null, int $code = -1): static
    {
        return new static(false, $message, $data, $code);
    }

    /**
     * 设置消息
     * @param string $message
     * @param bool $append
     * @return $this
     */
    public function setMessage(string $message, bool $append = false): static
    {
        $this->message = $append ? "$this->message$message" : $message;
        return $this;
    }

    /**
     * 设置内部消息
     * @param string $innerMessage
     * @return $this
     */
    public function setInnerMessage(string $innerMessage): static
    {
        $this->innerMessage = $innerMessage;
        return $this;
    }

    /**
     * 设置数据
     * @param T $data
     * @return $this
     */
    public function setData(mixed $data): static
    {
        $this->data = $data;
        return $this;
    }

    /**
     * 设置状态码
     * @param int $code
     * @return $this
     */
    public function setCode(int $code): static
    {
        $this->code = $code;
        return $this;
    }

    /**
     * 重写 __toString, 返回实例的 JSON 文本形式
     * @return string
     */
    public function __toString(): string
    {
        return $this->toJson();
    }

    /**
     * 返回实例的JSON文本形式
     * @param bool $withInnerMessage 是否包含 InnerMessage
     * @return string
     */
    public function toJson(bool $withInnerMessage = false): string
    {
        return json_encode($this->toArray($withInnerMessage), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_BIGINT_AS_STRING);
    }

    /**
     * 返回实例的数组形式
     * @param bool $withInnerMessage 是否包含 InnerMessage
     * @return array
     */
    #[ArrayShape(['success' => "bool", 'message' => "string", 'data' => "mixed", 'code' => "int", 'innerMessage' => "string"])]
    public function toArray(bool $withInnerMessage = false): array
    {
        $array = [
            'success' => $this->success,
            'message' => $this->message,
            'data'    => $this->data,
            'code'    => $this->code,
        ];
        if ($withInnerMessage) $array['innerMessage'] = $this->innerMessage;
        return $array;
    }

    /**
     * 转换为 ExecuteResult
     * @return ExecuteResult
     */
    public function toExecuteResult(): ExecuteResult{
        return new ExecuteResult($this->success,$this->message,$this->data);
    }

    /**
     * 重写 __invoke, 返回实例是否成功
     * @return bool
     */
    public function __invoke(): bool
    {
        return $this->success;
    }
}