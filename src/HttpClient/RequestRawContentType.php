<?php
/**
 * Product: ASW.Utility.
 * Date: 2023-09-9
 * Time: 16:14
 */

namespace ASW\Utility\HttpClient;


enum RequestRawContentType: string
{
    case Stream = 'application/octet-stream';

    case Json = 'application/json';

    case Text = 'text/plain';

    case Xml = 'text/xml';
}