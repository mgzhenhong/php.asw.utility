<?php
/**
 * Product: ASW.Utility.
 * Date: 2023-09-8
 * Time: 22:02
 */

namespace ASW\Utility\HttpClient;


enum PostFormat: string
{
    case URL_ENCODED = 'application/x-www-form-urlencoded';

    case FORM_DATA = 'multipart/form-data';
}