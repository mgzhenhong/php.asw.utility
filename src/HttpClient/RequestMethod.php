<?php
/**
 * Product: ASW.Utility.
 * Date: 2023-09-8
 * Time: 21:58
 */

namespace ASW\Utility\HttpClient;


enum RequestMethod: string
{
    case GET = 'GET';

    case POST = 'POST';

    case PUT = 'PUT';

    case  DELETE = 'DELETE';

    case HEAD = 'HEAD';
}