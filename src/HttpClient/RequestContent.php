<?php
/**
 * Product: ASW.Utility.
 * Date: 2023-09-9
 * Time: 21:39
 */

namespace ASW\Utility\HttpClient;


use ASW\Utility\HttpClient\RequestContent\FormData;
use ASW\Utility\HttpClient\RequestContent\Raw;
use ASW\Utility\HttpClient\RequestContent\UrlEncoded;

abstract class RequestContent
{
    public static function createFormDataContent(): FormData
    {
        return new FormData();
    }

    public static function createUrlEncodedContent(): UrlEncoded
    {
        return new UrlEncoded();
    }

    public static function createRawContent(): Raw
    {
        return new Raw();
    }

    abstract function getContentType(): string;

    abstract function getContent(): string|array;
}