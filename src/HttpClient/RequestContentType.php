<?php
/**
 * Product: ASW.Utility.
 * Date: 2023-09-9
 * Time: 16:14
 */

namespace ASW\Utility\HttpClient;


enum RequestContentType: string
{
    case URL_ENCODED = 'application/x-www-form-urlencoded';

    case FORM_DATA = 'multipart/form-data';

    case Json = 'application/json';

    case Text = 'text/plain';

    case Xml = 'text/xml';
}