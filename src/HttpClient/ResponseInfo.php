<?php

namespace ASW\Utility\HttpClient;

/**
 * Class ResponseInfo
 *
 * @package lib\Util\HttpClient
 *
 * @property-read string $url
 * @property-read string $contentType
 * @property-read int $httpCode
 * @property-read int $headerSize
 * @property-read int $requestSize
 * @property-read int $filetime
 * @property-read int $sslVerifyResult
 * @property-read int $redirectCount
 * @property-read float $totalTime
 * @property-read float $namelookupTime
 * @property-read float $connectTime
 * @property-read float $pretransferTime
 * @property-read int $sizeUpload
 * @property-read int $sizeDownload
 * @property-read float $speedDownload
 * @property-read float $speedUpload
 * @property-read int $downloadContentLength
 * @property-read int $uploadContentLength
 * @property-read float $starttransferTime
 * @property-read float $redirectTime
 * @property-read string $redirectUrl
 * @property-read string $primaryIp
 * @property-read array $certinfo
 * @property-read int $primaryPort
 * @property-read string $localIp
 * @property-read int $localPort
 */
class ResponseInfo
{
    private array $_properties       = [];
    private array $_curlResponseInfo = [];

    public static function fromCurlResponseInfo(array $curlResponseInfo): self
    {
        $instance                    = new self();
        $instance->_curlResponseInfo = $curlResponseInfo;

        foreach ($curlResponseInfo as $key => $value) {
            $propertyKey                         = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $key))));
            $instance->_properties[$propertyKey] = $value;
        }

        return $instance;
    }

    public function __get(string $propertyName)
    {
        if (!array_key_exists($propertyName, $this->_properties)) return null;

        return $this->_properties[$propertyName];
    }

    public function dump(): string
    {
        return json_encode(['curlResponseInfo' => $this->_curlResponseInfo, 'properties' => $this->_properties], JSON_PRETTY_PRINT);
    }
}