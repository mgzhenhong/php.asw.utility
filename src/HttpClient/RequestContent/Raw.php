<?php
/**
 * Product: ASW.Utility.
 * Date: 2023-09-10
 * Time: 12:12
 */

namespace ASW\Utility\HttpClient\RequestContent;


use ASW\Utility\HttpClient\RequestContent;
use ASW\Utility\HttpClient\RequestRawContentType;

class Raw extends RequestContent
{
    private string $contentType = '';
    private string $content     = '';

    public function setJsonContent(string|array $json, int $jsonEncodeFlag = 0): static
    {
        $this->contentType = RequestRawContentType::Json->value;
        $this->content     = is_array($json) ? json_encode($json, $jsonEncodeFlag) : $json;
        return $this;
    }

    public function setTextContent(string $text): static
    {
        $this->contentType = RequestRawContentType::Text->value;
        $this->content     = $text;
        return $this;
    }

    public function setXmlContent(string $xml): static
    {
        $this->contentType = RequestRawContentType::Xml->value;
        $this->content     = $xml;
        return $this;
    }

    public function setStreamContent(string $binary): static
    {
        $this->contentType = RequestRawContentType::Stream->value;
        $this->content     = $binary;
        return $this;
    }

    public function setFileContent(string $fileName): static
    {
        $this->contentType = RequestRawContentType::Stream->value;
        $this->content     = file_get_contents($fileName);
        return $this;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    function getContent(): string|array
    {
        return $this->content;
    }

    public function setContent(string|RequestRawContentType $contentType, string $content): static
    {
        $this->contentType = is_string($contentType) ? $contentType : $contentType->value;
        $this->content     = $content;
        return $this;
    }
}