<?php
/**
 * Product: ASW.Utility.
 * Date: 2023-09-10
 * Time: 12:00
 */

namespace ASW\Utility\HttpClient\RequestContent;


use ASW\Utility\HttpClient\RequestContent;
use ASW\Utility\HttpClient\RequestContentType;
use CURLFile;

class FormData extends RequestContent
{
    private array $content = [];

    public function addItem(string $key, string|int|float|bool $value): static
    {
        $this->content[$key] = $value;
        return $this;
    }

    public function addUploadFile(string $key, string $fileName): static
    {
        $this->content[$key] = new CURLFile(realpath($fileName));
        return $this;
    }

    public function getContentType(): string
    {
        return RequestContentType::FORM_DATA->value;
    }

    function getContent(): string|array
    {
        return $this->content;
    }
}