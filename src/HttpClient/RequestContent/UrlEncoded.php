<?php
/**
 * Product: ASW.Utility.
 * Date: 2023-09-10
 * Time: 12:06
 */

namespace ASW\Utility\HttpClient\RequestContent;


use ASW\Utility\HttpClient\RequestContent;
use ASW\Utility\HttpClient\RequestContentType;

class UrlEncoded extends RequestContent
{
    private array $content = [];

    public function addItem(string $key, string|int|float|bool|array $value): static
    {
        if (array_key_exists($key, $this->content)) {
            $this->content[$key] = [
                $this->content[$key],
                $value
            ];
        } else {
            $this->content[$key] = $value;
        }

        return $this;
    }

    public function getContentType(): string
    {
        return RequestContentType::URL_ENCODED->value;
    }

    function getContent(): string|array
    {
        return http_build_query($this->content);
    }
}