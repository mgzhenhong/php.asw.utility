<?php
/**
 * Product: ASW.Utility.
 * Date: 2023-09-8
 * Time: 22:01
 */

namespace ASW\Utility\HttpClient;


class RequestHeaders
{
    const HEADER_CONTENT_TYPE    = 'Content-Type';
    const HEADER_USER_AGENT      = 'Content-Type';
    const HEADER_ACCEPT          = 'Accept';
    const HEADER_ACCEPT_LANGUAGE = 'Accept-Language';
}