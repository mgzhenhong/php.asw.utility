<?php

namespace ASW\Utility;


enum ExecuteResultFailCode: int
{
    /**
     * 成功
     */
    case SUCCESS = 0;
    /**
     * 登录成功
     */
    case LOGIN_SUCCESS = 201;
    /**
     * 无法理解的请求
     */
    case BAD_REQUEST = 400;
    /**
     * 需要登录
     */
    case NEED_LOGIN = 401;
    /**
     * @var int 参数错误: 必要的参数不存在或格式错误
     */
    case PARAM_ERROR = 402;
    /**
     * 操作逻辑有问题
     */
    case LOGIC_VERIFY_FAIL = 403;
    /**
     * 找不到请求的内容
     */
    case NOT_FOUND = 404;
    /**
     * 所有权检查失败
     */
    case OWNER_VERIFY_FAIL = 405;
    /**
     * 权限验证失败
     */
    case PERMISSION_DENIED = 406;
    /**
     * 请求过于频繁, 或状态正在更新, 等会再查询
     */
    case SERVICE_DELAY = 407;
    /**
     * 数据重复
     */
    case DATA_DUPLICATE = 410;
    /**
     * 数据为空
     */
    case DATA_EMPTY = 411;
    /**
     * 数据格式不对/尺寸超限
     */
    case DATA_FORMAT_ERROR = 412;
    /**
     * 执行了更新指令, 但数据未变化
     */
    case DATA_NOT_CHANGE = 413;
    /**
     * 资源不足, 需求超过上限
     */
    case LACK_OF_RESOURCE = 420;
    /**
     * 服务器自身错误
     */
    case SERVER_ERROR = 500;
    /**
     * 数据库操作错误
     */
    case DATABASE_ERROR = 510;
    /**
     * 依赖的外部资源获取失败
     */
    case DEP_RESOURCE_REQ_FAIL = 520;
    /**
     * 依赖的外部资源内容读取（解析）失败
     */
    case DEP_RESOURCE_CONTENT_FAIL = 521;
}