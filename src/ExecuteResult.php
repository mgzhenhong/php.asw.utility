<?php

namespace ASW\Utility;


use JetBrains\PhpStorm\ArrayShape;
use Stringable;

/**
 * 操作结果通用类
 * @template T
 */
class ExecuteResult implements Stringable
{

    /**-----------------------------**/
    /**  Static Region              **/
    /**
     * @var bool 处理结果是否成功
     */
    public bool $result;
    /**
     * @var string 处理结果的文本信息, 通常用来传递错误文本
     */
    public string $info;
    /**
     * @var string 处理结果的内部文本信息, 通常用来传递不交付给用户的内部信息, 例如用于记录详细日志
     */
    public string $innerInfo = '';
    /**
     * @var T 处理结果数据
     */
    public mixed $data;

    /**-----------------------------**/
    /**  Instance Region            **/
    /**-----------------------------**/

    /**
     * @param bool $result
     * @param string $info
     * @param T|null $data
     */
    public function __construct(bool $result, string $info = '', mixed $data = null)
    {
        $this->result = $result;
        $this->info   = $info;
        $this->data   = $data;
    }

    /**-----------------------------**/

    /**
     * 从 JSON 转换
     * @param string $json
     * @return static|null
     */
    public static function fromJson(string $json): ?static
    {
        if (null === $arr = json_decode($json, true)) return null;
        return self::fromArray($arr);
    }

    /**
     * 从数组转换
     * @param array $arr
     * @return static
     */
    public static function fromArray(array $arr): static
    {
        $result = boolval($arr['result'] ?? false);
        $info   = strval($arr['info'] ?? '');
        $data   = $arr['data'] ?? null;
        return new static($result, $info, $data);
    }

    /**
     * 从 ActionResult 转换
     * @param ActionResult $actionResult
     * @return static
     */
    public  static function fromActionResult(ActionResult $actionResult): static {
        return new static($actionResult->success, $actionResult->message, $actionResult->data);
    }

    /**
     * 成功
     * @param mixed|null $data
     * @param string $info
     * @return static
     */
    public static function success(mixed $data = null, string $info = ''): static
    {
        return new static(true, $info, $data);
    }

    /**
     * 失败
     * @param string $info
     * @param mixed|null $data
     * @return static
     */
    public static function fail(string $info = '', mixed $data = null): static
    {
        return new static(false, $info, $data);
    }

    /**
     * 设置 Info
     * @param string $info
     * @param bool $append
     * @return $this
     */
    public function setInfo(string $info, bool $append = false): static
    {
        $this->info = $append ? "$this->info$info" : $info;
        return $this;
    }

    /**
     * 设置 InnerInfo
     * @param string $innerInfo
     * @return $this
     */
    public function setInnerInfo(string $innerInfo): static
    {
        $this->innerInfo = $innerInfo;
        return $this;
    }

    /**
     * 设置 Data
     * @param T $data
     * @return $this
     */
    public function setData(mixed $data): static
    {
        $this->data = $data;
        return $this;
    }

    /**
     * 重写 __toString, 返回实例的JSON文本形式
     * @return string
     */
    public function __toString(): string
    {
        return $this->getJson();
    }

    /**
     * 返回实例的JSON文本形式
     * @param bool $withInnerInfo 是否包含InnerInfo
     * @return string
     */
    public function getJson(bool $withInnerInfo = false): string
    {
        return json_encode($this->getArray($withInnerInfo), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_BIGINT_AS_STRING);
    }

    /**
     * 返回实例的数组形式
     * @param bool $withInnerInfo 是否包含InnerInfo
     * @return array
     */
    #[ArrayShape(['result' => "bool", 'info' => "string", 'data' => "mixed", 'innerInfo' => "string"])]
    public function getArray(bool $withInnerInfo = false): array
    {
        $array = [
            'result' => $this->result,
            'info'   => $this->info,
            'data'   => $this->data,
        ];
        if ($withInnerInfo) $array['innerInfo'] = $this->innerInfo;
        return $array;
    }

    /**
     * 返回实例的 ActionResult 形式
     * @return ActionResult
     */
    public function  getActionResult(): ActionResult{
        return new ActionResult($this->result,$this->info,$this->data);
    }

    /**
     * 重写 __invoke, 返回实例是否成功
     * @return bool
     */
    public function __invoke(): bool
    {
        return $this->isSuccess();
    }

    /**
     * 返回实例是否成功
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->result;
    }
}