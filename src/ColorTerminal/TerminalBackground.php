<?php

namespace ASW\Utility\ColorTerminal;


enum TerminalBackground: string
{
    case BG_COLOR_BLACK      = '40';
    case BG_COLOR_RED        = '41';
    case BG_COLOR_GREEN      = '42';
    case BG_COLOR_YELLOW     = '43';
    case BG_COLOR_BLUE       = '44';
    case BG_COLOR_MAGENTA    = '45';
    case BG_COLOR_CYAN       = '46';
    case BG_COLOR_LIGHT_GRAY = '47';
}