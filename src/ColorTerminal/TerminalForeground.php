<?php

namespace ASW\Utility\ColorTerminal;


enum TerminalForeground: string
{
    case TXT_COLOR_BLACK        = '0;30';
    case TXT_COLOR_DARK_GRAY    = '1;30';
    case TXT_COLOR_BLUE         = '0;34';
    case TXT_COLOR_LIGHT_BLUE   = '1;34';
    case TXT_COLOR_GREEN        = '0;32';
    case TXT_COLOR_LIGHT_GREEN  = '1;32';
    case TXT_COLOR_CYAN         = '0;36';
    case TXT_COLOR_LIGHT_CYAN   = '1;36';
    case TXT_COLOR_RED          = '0;31';
    case TXT_COLOR_LIGHT_RED    = '1;31';
    case TXT_COLOR_PURPLE       = '0;35';
    case TXT_COLOR_LIGHT_PURPLE = '1;35';
    case TXT_COLOR_BROWN        = '0;33';
    case TXT_COLOR_YELLOW       = '1;33';
    case TXT_COLOR_LIGHT_GRAY   = '0;37';
    case TXT_COLOR_WHITE        = '1;37';
}