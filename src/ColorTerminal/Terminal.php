<?php

namespace ASW\Utility\ColorTerminal;


class Terminal
{
    /**
     * 创建一个新实例
     *
     * @return static
     */
    public static function create(): static
    {
        return new static();
    }

    /**
     * 高亮
     *
     * @return static
     */
    public function highLight(): static
    {
        echo "\033[1m";
        return $this;
    }

    /**
     * 下划线
     *
     * @param bool $underline
     *
     * @return static
     */
    public function underline(bool $underline = true): static
    {
        echo "\033[4m";
        return $this;
    }

    /**
     * 闪烁
     *
     * @return static
     */
    public function flash(): static
    {
        echo "\033[5m";
        return $this;
    }

    /**
     * 反显
     *
     * @return static
     */
    public function reverseColor(): static
    {
        echo "\033[7m";
        return $this;
    }

    /**
     * 消隐
     *
     * @return static
     */
    public function cancelHide(): static
    {
        echo "\033[8m";
        return $this;
    }

    /**
     * 光标上移
     *
     * @param int $size
     *
     * @return static
     */
    public function cursorMoveUp(int $size): static
    {
        echo "\033[{$size}A";
        return $this;
    }

    /**
     * 光标下移
     *
     * @param int $size
     *
     * @return static
     */
    public function cursorMoveDown(int $size): static
    {
        echo "\033[{$size}B";
        return $this;
    }

    /**
     * 光标右移
     *
     * @param int $size
     *
     * @return static
     */
    public function cursorMoveRight(int $size): static
    {
        echo "\033[{$size}C";
        return $this;
    }

    /**
     * 清屏
     */
    public function clearScreen(bool $resetCursorPos = true): static
    {
        echo "\033[2J";
        if ($resetCursorPos) self::setCursorPos(0, 0);
        return $this;
    }

    /**
     * 置光标位置
     *
     * @param int $x
     * @param int $y
     *
     * @return static
     */
    public function setCursorPos(int $x, int $y): static
    {
        echo "\033[{$y};{$x}H";
        return $this;
    }

    /**
     * 清除当前行
     *
     * @return static
     */
    public function clearCurrentRow(): static
    {
        self::cursorMoveLeft(600);
        self::clearAfterCursor();
        return $this;
    }

    /**
     * 光标左移
     *
     * @param int $size
     *
     * @return static
     */
    public function cursorMoveLeft(int $size): static
    {
        echo "\033[{$size}D";
        return $this;
    }

    /**
     * 清除从光标到行尾的内容
     *
     * @return static
     */
    public function clearAfterCursor(): static
    {
        echo "\033[K";
        return $this;
    }

    /**
     * 保存光标位置
     *
     * @return static
     */
    public function cacheCursorPos(): static
    {
        echo "\033[s";
        return $this;
    }

    /**
     * 恢复光标位置
     *
     * @return static
     */
    public function resumeCursorPos(): static
    {
        echo "\033[u";
        return $this;
    }

    /**
     * 隐藏光标
     *
     * @return static
     */
    public function hideCursor(): static
    {
        echo "\033[?25l";
        return $this;
    }

    /**
     * 显示光标
     *
     * @return static
     */
    public function showCursor(): static
    {
        echo "\033[?25h";
        return $this;
    }

    /**
     * 输出
     * @param string $text
     * @param bool $autoEnd
     * @return $this
     */
    public function echo(string $text, bool $autoEnd = true): static
    {
        echo $text;
        if ($autoEnd) $this->end();
        return $this;
    }

    /**
     * 清除所有属性
     *
     * @return static
     */
    public function end(): static
    {
        echo "\033[0m";
        return $this;
    }

    /**
     * 设置前景色
     *
     * @param TerminalForeground $color
     * @return static
     */
    public function color(TerminalForeground $color): static
    {
        echo "\033[{$color->value}m";
        return $this;
    }

    /**
     * 设置背景色
     *
     * @param TerminalBackground $color
     * @return static
     */
    public function bgColor(TerminalBackground $color): static
    {
        echo "\033[{$color->value}m";
        return $this;
    }
}