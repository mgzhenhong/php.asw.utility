<?php

namespace ASW\Utility\Enums;


enum PhpErrorLevel: int
{
    case E_ERROR             = 0x0001;
    case E_WARNING           = 0x0002;
    case E_PARSE             = 0x0004;
    case E_NOTICE            = 0x0008;
    case E_CORE_ERROR        = 0x0010;
    case E_CORE_WARNING      = 0x0020;
    case E_COMPILE_ERROR     = 0x0040;
    case E_COMPILE_WARNING   = 0x0080;
    case E_USER_ERROR        = 0x0100;
    case E_USER_WARNING      = 0x0200;
    case E_USER_NOTICE       = 0x0400;
    case E_STRICT            = 0x0800;
    case E_RECOVERABLE_ERROR = 0x1000;
    case E_DEPRECATED        = 0x2000;
    case E_USER_DEPRECATED   = 0x4000;
    case E_ALL               = 0x7FFF;
}