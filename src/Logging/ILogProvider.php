<?php

namespace ASW\Utility\Logging;


use Throwable;

interface ILogProvider
{
    public function log(LogLevel $logLevel, string $category, string $message, Throwable $throwable = null): void;
}