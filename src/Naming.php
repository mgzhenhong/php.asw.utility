<?php

namespace ASW\Utility;

class Naming
{
    /**
     * camelCase 命名法
     *
     * @param string $str
     *
     * @return string
     */
    public static function camelCase(string $str): string
    {
        $str = self::pascalCase($str);
        return strtolower($str[0]) . substr($str, 1);
    }

    /**
     * PascalCase 命名法
     *
     * @param string $str
     *
     * @return string
     */
    public static function pascalCase(string $str): string
    {
        $str = preg_replace("/[^a-z0-9]/i", ' ', $str);
        $str = implode('', array_udiff(str_split(ucwords($str)), str_split($str), fn($a, $b) => $a > $b ? 1 : -1));
        return preg_replace("/[^a-z0-9]/i", '', $str);
    }
}